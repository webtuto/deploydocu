---
sidebar_position: 3
---
# Render - Déploiement - Iaac
Nous détaillerons comment créer et maintenir un service avec de l'Insfrastructure As A Code. Render permet d'utiliser un GUI depuis la partie *Dashboard* que nous ne détaillerons pas ici. 
Quelques caractéristiques :
* Automatisation par défaut de la partie delivery
* *Render* se charge du build, nous fournissons un *Dockerfile*. 
* Pas de gestion de repository. 
* *Render* peut déployer du code directement 
* Les services se déploient et maintiennent depuis un fichier *render.yaml*
## Retour d'expérience 
De 1er abord, *render* est facilement opérationel. La partie mise en production est pensé pour du *Gitops* et viens se greffer rapidement à *Gitlab*, mon outil de prédilection. Pas d'étape de *build* à configurer, n'y de repo à gérer. Ces étapes étant nécessaires, *Render* les gères sans que nous ayons de vue sur ce qu'il fait. Pratique ? peut être un peu trop. 
## Déployer un service - Iaac
Un service se configure et maintient avec les [bueprint spec](https://render.com/docs/blueprint-spec). Les spécifications se fait dans la balise `services`, qui a un `type`et des attributs.
Les `type` de services :
* `web` for a web service
* `worker` for a background worker
* `pserv` for a private service
* `cron` for a cron job

Chaque `type` de service est exécuté dans un environnement qui peut être un conteneur, le langage utilisé, ou un fichier statique : `docker`, `elixir`, `go`, `node`,`python`, `ruby`, `rust`, `static`

Il est possible de spécifier des options pour *docker* :
```YAML
dockerfilePath: ./sub/Dockerfile
dockerContext: ./sub/src
```
Il est possible de spécifier un nombre d'instance ou une mise à l'échelle automatique: 
```YAML
scaling:
  minInstances: 1
  maxInstances: 3
  targetMemoryPercent: 60 # optional if targetCPUPercent is set (valid: 1-90)
  targetCPUPercent: 60 # optional if targetMemory is set (valid: 1-90)
```

Sans mise à l'échelle, spécifier le nombre voulu d'instance :
```YAML
numInstances: 3
```

Les *variable d'envrionnement* s'écrivent de la forme `KEY: VALUE` et peuvent dépendre de propriétés d'autre services (web, base de données notamment). [Pour plus de propriété](https://render.com/docs/blueprint-spec#properties-available-to-environment-variables)
```YAML
key: DB_URL
fromDatabase:
  name: prod
  property: connectionString
key: REDIS_URL
fromService:
  type: redis
  name: lightning
  property: connectionString
```

Il est possible de paratager les clefs avec un groupe, `fromGroup: my-env-group`. Il est possible de les générer : 
```YAML
key: APP_SECRET
generateValue: true
```
Les `repo` et `branch`. A la création d'un service avec blueprint depuis le GUI - *Dashboard* sur *render.com*, il est demandé un repo. *Render* propose une intégration *Gitlab* et *Github*. Il est possible de le spécifier dans le fichier *render.yaml* : 
```YAML
 - type: web
    name: webdis
    env: docker
    repo: https://github.com/render-examples/webdis.git # optional
``` 


### web service - Déploiment d'un site en statique

Déploiement d'un site en statique :

``YAML
services:
 - type: web
   name: pablogrondindoc
   env: static
   buildCommand: yarn build
   staticPublishPath: ./build
```

Il est possible de spécifier les `routes` et les `headers` : 
```YAML
routes:
  # redirect (HTTP status 301) from /a to /b
  - type: redirect
    source: /a
    destination: /b
  # rewrite /app/* requests to /app
  - type: rewrite
    source: /app/*
    destination: /app
```
### redis service 
Les instances rédis doivent contenir au minimum un `type`, `nom` et une `ipAllowlist` .Le reste est optionnel
```YAML
services:
  # Redis 1 - all fields defined
  - type: redis
    name: thunder
    ipAllowList: # required - allow external connections from only these CIDR blocks
      - source: 203.0.113.4/30
        description: office
      - source: 198.51.100.1
        description: home
```

### Déploiement d'une base de données
A besoin seulement d'un `nom` pour être lancé 
```YAML
databases:
  # db 1
  - name: staging
  # db 2
  - name: prod
    region: frankfurt
    plan: pro
    databaseName: prod_app
    user: app_user
    ipAllowList: # optional (defaults to allow all)
      - source: 203.0.113.4/30
        description: office
      - source: 198.51.100.1
        description: home
  # db 3
  - name: private database
    databaseName: private
    ipAllowList: [] # only allow internal connections
    postgresMajorVersion: 13 # optional (defaults to 13, supported versions are 11, 12, 13, and 14)
```

## Cas d'usages
### Elastic search
* Le template par défaut proposé par [render](https://github.com/render-examples/elasticsearch)
   * `10GB` par défaut
   * Sur le réseau de render privée
* Nécéssite le dossier `config` dans le lien ci-dessus pour la configuration de elastic search :
```YAML
services:
- type: pserv
  name: elasticsearch
  env: docker
  plan: standard
  disk:
    name: esdata
    mountPath: /usr/share/elasticsearch/data
    sizeGB: 10
  autoDeploy: false # so ES does not restart on a push to this repository.
  envVars:
  - key: ES_JAVA_OPTS
    value: 'Xms512m Xmx512m'
  - key: discovery.type
    value: single-node
  - key: cluster.name
    value: elastic
```
Avec le dockerfile : 
```YAML
# The official Elasticsearch Docker image
FROM docker.elastic.co/elasticsearch/elasticsearch:7.16.1@sha256:1000eae211ce9e3fcd1850928eea4ee45a0a5173154df954f7b4c7a093b849f8

# Copy our config file over
COPY --chown=1000:0 config/elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml

# Allow Elasticsearch to create `elasticsearch.keystore`
# to circumvent https://github.com/elastic/ansible-elasticsearch/issues/430
RUN chmod g+ws /usr/share/elasticsearch/config

USER 1000:0
```

Le fichier de configuration idans `/config/elasticsearch.yml` : 

```YAML
cluster.name: "elastic"
# bind to all network interfaces
network.host: 0.0.0.0
```
## Gestion des logs
Les logs des services peuvent être transferés sur TLS-enabled syslog. Il ne semble pas possible de spécifier en *Iaac* les configurations des logs. 

