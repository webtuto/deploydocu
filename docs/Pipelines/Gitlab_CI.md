---
sidebar_position: 1
---

# Gitlab CI

 [La plateforme Gitlab](https://docs.gitlab.com/) permet de construire des *pipelines* `CI/CD`  de façon automatique, semi-automatique ou manuel :
* Les outils de [CI/CD](https://docs.gitlab.com/ee/ci/)

* [Auto devops](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/) afin d'automatiserles `build`, `test`, `deploy`, et `monitor` des applications
* [Gitlab Runner](https://docs.gitlab.com/runner/). Application écrit en `Go` qui exécute les tâches dit `jobs` des *pipelines*. Diponibles dans le cloud ou sur site.

## Quelques bases
* La configurations se fait avec le fichier `.gitlab-ci.yml` écrit en `YAML`. Il doit se localiser à racine du projet. [Liste des mots clefs de configurations](https://docs.gitlab.com/ee/ci/yaml/index.html#needs)
* `Pipelines` : Composant de plus hauts niveaux du `CI/CD`. Ils comprennent :
   * Les *jobs* ou *taches*, c'est l'action a effectuer. Compiler ou tester du code par exemple
      * Exécuter par les *runners*
      * Tous les *jobs* d'un même *stage* sont exécutés en parrallèle s'il y a assez de *runners*
      * Tous les *jobs* doivent réussir pour passer à la prochaine étape 
   * Les *stages* ou *étapes*, qui est l'étape a effectuer. C'est le *quand* l'action doit être faite. Par exemple *stage* de build avant le *stage* qui de tests.
* [Il y a plusieurs types](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#basic-pipelines) de *Pipelines*. En voici quelque une :
   * *Direct Acyclic Graph* - DAG utilisé afin d'amélirer les performances. Les *jobs* peuvent s'éxécuter les uns parès les autres sans attendre la fin de tous les *jobs* d'un *stages* 
      * Utile le mot clef `needs`pour mettre en relation les *jobs*.
      * [Exemples](https://docs.gitlab.com/ee/ci/yaml/index.html#needs)
   * *Child / Parent* 
      * Utilise le mot clef `trigger`. Permet de séparer la configuration et de les réunirs dans un seul fichier. S'utilise généralement avec `include`
   * *Branch pipeline*, C'est la *pipeline* **par défaut** lase lance lors d'un *commit*   
   * Les *merge request pipelines*. La *pipeline* se lance lors d'une [requête de merge](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)
      * `rules`
      * ``only/except`
   * *Merge result pipelines* qui s'éxécutent en fonction du résultat du *merge* de deux branches. 

## Analyse de fichier `.gitlab-ci.yml`

* Ci-dessous, une *Pipepline* fonctionnelle à 2 étapes - *stage* : `test` et `deploy`
   * Ces deux étapes ne sont pas directement relié :
      * Nous avons un *branch pipeline* qui s'éxécute lors d'un commit, c'est le stage *test*
      * Nous avons un *merge pipeline* qui s'éxécute lors d'un merge sur *page*
         * mot clef utilisé `only`
         * Possible utilisation de `rule` 
      * Cependant, si la pipeline de *test* échoue, nous aurons un *warning* lors du *merge* 
* Le fichier de fonctionnel
```YAML
image: node:18-alpine
#image: node:15.12-alpine3.13
stages:
  - test
  - deploy

test:
  stage: test
  script:
    - npm install --force
    - npm run build

pages:
  stage: deploy
  script:
    - npm install --force
    - npm run build docu
    - mv ./build ./public
  artifacts:
    paths:
    - public
  only:
    - pages
```

* Utilisation de `rule` et `if` pour définir notre *merge request pipeline*. 
:::warning
La configuration ci-dessous effectue lors d'un *merge* un `deploy` *puis* `test`
:::: 
```YAML
pages:
  stage: deploy
  script:
    - npm install --force
    - npm run build
    - mv ./build ./public
  artifacts:
    paths:
    - public
  # Les 2 if si dessous fonctionne
  rules:
    # Merge request from master to pages
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^master/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "pages"
```
* Pour déclencher à chaque merge
```bash

    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME == "pages"
   
```
### Améliroation de notre pipeline 
* Séparons les étapes `install`, `build` et ajoutons une étapes de `test` avec *eslint*
* C'est les `artifact` qui vont nous faire passer les objets entre les états 
* On spécifie dans `dependencie`, les objets dont ont hérite
:::warning
La durée de la pipeline explose à chaque étape !
::: 

```YAML

image: node:18-alpine
#image: node:15.12-alpine3.13
stages:
  - install 
  - build
  - test
  - deploy

install:
  stage: install
  script:
      - npm install --force
      - npm install eslint --save-dev
  artifacts:
    paths:
        - .npm/
        - node_modules/
    expire_in: 15 mins

build:
    stage: build
    script:
        - npm run build
    artifacts:
      paths:
         - build
      expire_in: 30 mins
    dependencies:
      - install
    

test:
  stage: test
  script:
  - npx eslint 'src/**/*.{js,jsx}'
  dependencies:
    - install
    - build

deploy:
    stage: deploy
    script:
    - mv ./build ./public
    artifacts:
        paths:
            - public
    only:
        - pages
    dependencies:
        - build

```
:::tips
On garde l'install et le build dans la meme etape
:::

## Quelque note
* `artifacts` permet de faire sauvegarder et passer du contenu entre les *stages*
* les `image`peuvent être utilisées dans chaque `stage`. Exemple [openclassroom](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops/6182908-integrez-votre-code-en-continu)
* Le mot clef `cache` peut accélérer les étapes si plusieurs jobs utilisent le même répertoire
```YAML
stages:
  - build
  - test

cache:
  paths:
    - .m2/repository
  key: "$CI_JOB_NAME"

build_job:
  stage: build
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine

test_job:
  stage: test
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
  image: openjdk:8-alpine
```

## Quelques actuce
* [Les templates qui peuvent aider !](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates)