---
sidebar_position: 2
---

# Google Cloud - Déploiement
## Cloud Run 
Avec de l'Infrastructure As A Code, nous verrons comment créer et déployer un service 
   * Avec `Terraform` 
   * Avec `knative`

L'ensemble de l'infrastructure va se gérer avec 2 fichiers : `cloudconfig.yaml`, `main.tf` ou `service.yaml`. GCP met aussi à disposition une CLI et un GUI. 

**Prérequis** :
* S'authentifier :
```bash
gcloud auth configure-docker europe-west9-docker.pkg.dev
```
* Des images disponibles sur l'artifact repository (Cf Conteneurisation > Build)

### Retour d'expérience Cloud Run
Google Cloud propose une offre très complète de service cloud, tout est possible, configurable. 
Mais justement, **tout** doit être configuré, l'authentification depuis Gitlab est délicate. Tant que la configuration s'effectue depuis la Google Cloud Console en locale ou remote, pas de problème. Mais l'intégratio à *Gitlab* n'est pas aisé, notamment **from scratch**. En effet, les services doivent être configuré avec de l'IAM (compte de service, role). 

### Création d'un service avec `Terraform`
* [Liens pour la documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#example-usage---cloud-run-service-basic)
* Depuis la console GCP cloud, création d'un service avec les droits IAM pour accèder depuis l'extérieur :
```YAML
provider "google" {
    project = "iaacgitlab"
}

resource "google_cloud_run_service" "default" {
    name     = "pablogrondin"
    location = "europe-west9"

    metadata {
      annotations = {
        "run.googleapis.com/client-name" = "terraform"
      }
    }

    template {
      spec {
        containers {
          image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"
        }
      }
    }
 }

 data "google_iam_policy" "noauth" {
   binding {
     role = "roles/run.invoker"
     members = ["allUsers"]
   }
 }

 resource "google_cloud_run_service_iam_policy" "noauth" {
   location    = google_cloud_run_service.default.location
   project     = google_cloud_run_service.default.project
   service     = google_cloud_run_service.default.name

   policy_data = data.google_iam_policy.noauth.policy_data
 }
```
Depuis la console GCP, utiliser `terraform init`, `plan`, `apply`
### Création d'un service avec knative
Parmis les façon de configurer GCP, il est possible d'utiliser les services.yaml. De nombreuses configurations par défaut comme les limites CPU, Mémoire n'ont pas besoins d'être spécifié.Nous additionons une révision 0.1.2 ci dessous. 
```YAML
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: deploydocu

spec:
  template:
    metadata:
      name: 0.1.2
    spec:

      containers:
      - image: europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5 
```
* Création du service :
```bash
gcloud run services replace service.yaml
```
## Google Cloud Automation
[Google Cloud automationr](https://cloud.google.com/stackdriver/docs/solutions/agents/ops-agent/fleet-installation]. Anciennement deux agents (toujours supporté) agent monitoring et logging qui sont maintenant rassemblé en un agent,
[Agent ops](https://cloud.google.com/stackdriver/docs/solutions/agents/ops-agent) qui collect de la télémétrie journalisation et métrique système à partir des instances Compute Engine. Il utilise [Fluent bit](https://fluentbit.io/).

Agent ops propose plusieurs méthodes d'installation, avec gcloud sur un parc de VM :
* [gcloud](https://cloud.google.com/stackdriver/docs/solutions/agents/ops-agent/managing-agent-policies)
* A l'aide d'outil d'automatisation :
   * [Ansible](https://github.com/GoogleCloudPlatform/google-cloud-ops-agents-ansible#ansible-role-for-cloud-ops)
   * [Chef](https://github.com/GoogleCloudPlatform/google-cloud-ops-agents-chef/blob/master/README.md)
   * Terraform, pour les VM disposant de [modules compatibles](https://cloud.google.com/stackdriver/docs/solutions/agents/ops-agent/managing-agent-policies#supported_operating_systems)
   * La CLI



