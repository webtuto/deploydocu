---
sidebar_position: 1
---
# Truc et astuce 
## Enregistrer les entrées et sorties du terminal

La commande `script`permet d'enregistrer les commandes **et** les résultats. Pour arréter l'enregistrement, `ctrl+d`ou `exit` 

* Sans argument, un fichier nommé *typescript* est crée dans le répertoire courant `/home/<NOM_UTILISATEUR>`de l'utilisateur

```bash
pi@raspberrypi:~ $ ls /home/pi/
Bookshelf  Desktop  Documents  Downloads  Music  Pictures  Public  resultats  Templates  Videos
pi@raspberrypi:~ $ script
Script started, output log file is 'typescript'.
pi@raspberrypi:~ $ ls /home/pi/
Bookshelf  Desktop  Documents  Downloads  Music  Pictures  Public  resultats  Templates  typescript  Videos
pi@raspberrypi:~ $ exit

```
* `script <NOM_FICHIER>`  permet de préciser le nom de fichier et de le localiser. 

```bash
pi@raspberrypi:~ $ pwd
/home/pi
pi@raspberrypi:~ $ ls
Bookshelf  Desktop  Documents  Downloads  Music  Pictures  Public  resultats  Templates  Videos
pi@raspberrypi:~ $ script exemple.log
Script started, output log file is 'exemple.log'.
pi@raspberrypi:~ $ ls
Bookshelf  Desktop  Documents  Downloads  exemple.log  Music  Pictures  Public  resultats  Templates  Videos
pi@raspberrypi:~ $ exit
exit
Script done.
```
* Option `-a` *append* pour ajouter le résultat à un fichier existant.  

```bash
# Creation d un fichier avec du contenu
pi@raspberrypi:~ $ echo "Creation fichier pour option -a dans script" >> /home/pi/exemple.log
pi@raspberrypi:~ $

# On lance script avec le fichier exemple.txt et quelque commmande 
pi@raspberrypi:~ $ script -a /home/pi/exemple.log
Script started, output log file is '/home/pi/exemple.log'.
pi@raspberrypi:~ $ ls /home/
compose  pi  website
pi@raspberrypi:~ $ exit
exit
Script done.

# Le fichier exemple.log doit contenir notre commentaire avec echo et la commande effectué : ls

pi@raspberrypi:~ $ cat /home/pi/exemple.log
Creation fichier pour option -a dans script
Script started on 2022-06-23 18:47:28+01:00 [TERM="xterm" TTY="/dev/pts/4" COLUMNS="180" LINES="42"]
pi@raspberrypi:~ $ ls /home/
compose  pi  website
pi@raspberrypi:~ $ exit
exit

```
### Automatiser `script`

A chaque ouverture de terminal par l'utilisateur voulu, la commande `script`s executera. Dans le fichier `/hom/<NOM_UTILISATEUR>/.bashrc`, ajouter les commandes 
* Le résultat sera sauvegardé dans les logs, l'utilisateur doit donc avoir accès aux fichiers de log en écriture 
```bash
if [[ -z $SCRIPT ]]; then
  now=$(date +"%m_%d_%Y_%H:%M:%S")
  me=$(whoami)
  export SCRIPT=/var/log/script/$me.$now.log
  script "$SCRIPT"
fi

```
:::warning Attention !
Le résultat sera sauvegardé dans les logs, l'utilisateur doit donc avoir accès aux fichiers de log en écriture
::: 

## L'historique 

1. Ajouter la date et l'heure
* Ouvrir le fichier de configuration `.bashrc`
* Ajouter la ligne `HISTTIMEFORMAT="%Y-%m-%d %T "`
2. Rejouer l'historique sans taper de commande, depuis la console
* `!<NUM_LIGNE>`
```bash
website@raspberrypi:~/pablogrondin-doc/docs/system $ history 10
   41  2022-06-20 15:27:46 cd ../system/
   42  2022-06-20 15:27:52 touch demo.md
   43  2022-06-20 15:30:41 cd ../tutorial-extras/
   44  2022-06-20 15:30:41 ls
   45  2022-06-20 15:30:47 vi manage-docs-versions.md
   46  2022-06-20 15:31:05 cd -
   47  2022-06-20 15:31:10 vi demo.md
   48  2022-06-20 15:35:54 ls
   49  2022-06-20 15:36:04 history -n 10
   50  2022-06-20 15:36:07 history 10
# Pour rejouer la commande ls
website@raspberrypi:~/pablogrondin-doc/docs/system $ !48
ls
_category_.json  demo.md
```
## Mise en forme d'un résultat de commande

Les résulats de certaines commande comme `mount` ou les commandes `docker` sont illisibles

* `mount | columns -t`
```bash
/dev/mmcblk0p2  on  /                                                                                                 type  ext4             (rw,noatime)
devtmpfs        on  /dev                                                                                              type  devtmpfs         (rw,relatime,size=439400k,nr_inodes=109850,mode=755)
sysfs           on  /sys                                                                                              type  sysfs            (rw,nosuid,nodev,noexec,relatime)
proc            on  /proc                                                                                             type  proc             (rw,relatime)
```
