---
sidebar_position: 1
---

# Hyperviseur type 1
## Proxmox

Proxmox est une solution open source de gestion de machine virtuel KVM et conteneur LXC. Rien de vous empeche 
d'utiliser docker dans une VM linux et ou d'utiliser des solutions d'hyperviseur de type 1 ou 2 dans vos VM. 

Proxmox propose 3 solutions :
1. [Promox Virtual Environment- PVE](https://www.proxmox.com/en/proxmox-ve) 
2. [Proxmox Backup Server - PBS](https://www.proxmox.com/en/proxmox-backup-server)
3. [Proxmox Mail Gateway - PMG ](https://www.proxmox.com/en/proxmox-mail-gateway)

Pour chacune des solutions, proxmox propose des ISO, présentation des caractéristiques, [Admin guide, datasheet, bench mark ZFS et Ceph](https://www.proxmox.com/en/downloads/category/documentation-pve), un training ! 

PVE est la pierre angulaire des solutions Promox qui propose de créer, configurer, déployer des VM avec une solution de haute
disponibilité dit HA. 
## Proxmox Virtual Environnement 


## Caractéristiques


:::tip Pro
La technologie HA logiciel permet de se passer de redondance matérielle : RAID, double alimentation pour les VMs.
::: 

1. Centralisation de la gestion de son infrastructure via une interface web

2. Flexibilité des configurations possibles 
    * Gestion du cluster
    * Réplication
    * Stockage de données partagé
    * Backup
    * Gestion de l'authentication et des utilisateurs
    * Accès matériel pour les VM

3. L'interface est intuitive !

4. Solution open source ! 

:::warning Contre
De mauvaises configurations peuvent avoir des conséquences désastreuses
::: 

5. La documentation officielle recommande un hyperviseur *sans RAID matériel* pour les espaces de sotckage partagés `ZFS` ou `Ceph`.
Dans les faits, il y a quelques précautions à prendre mais cela ne pose aucun problème.

### Prérequis au HA
[Source - pve-admin-guide-7 - Chap. 15.1 ](https://www.proxmox.com/en/downloads/category/documentation-pve) : 
* Un cluster avec plus de 3 noeuds (Pour obtenir un quorum fiable)

* Un espace de stockage partagé pour les VMs et conteneurs

* Redondance matérielle partout 
   *  Utiliser un espace de stockage redondant et distribué pour les VMs- Comprendre `ZFS` ou `Ceph` 
   *  RAID pour le stockage local

* Utiliser des composants serveurs fiable
   *  Utiliser de la ECC-RAM
   *  Utiliser alimentations redondantes sur la carte mère
   *  Utiliser an uninterruptible power supply (UPS)

* hardware watchdog - si indisponible, utilisation du linux kernel software watchdog (softdog)


## Truc et astuce 

### Stockage ZFS

1. Créer le partage ZFS et l'ajouter au system de fichier 
* On cree le poll HD-Storage avec de la compression lz4
* **Prerequis** formatage avec `fdisk` de partition vide avec option `-n`
```bash
zpool create -o ashift=12 HD-Storage /dev/sda4
zfs set compression=lz4 HD-Storage
On vérifie que ça a bien marché 
zpool list
zpool get all | grep comp
```

2. Ajouter un disque a ZFS
* **Prerequis** : partition cree avec fdisk /dev/<"NOM_DISK"> et option `-n`
```bash
zpool add <NOM_ZPOOL> /dev/<PARTITION>
#Soit
zpool add HD-Storage /dev/sda1
```
3. Probleme replication Stockage ZFS
* On peut initier la replication : `pvesr run -id 300-0 --verbose`, 300 étant le numéro de la VM.


4. Detruire des disques virtuel 
```bash 
# DETRUIRE DES DISQUES VIRTUELS
root@pve1:/etc/pve/qemu-server# zfs destroy -f HD-Storage/vm-300-disk-2
root@pve1:/etc/pve/qemu-server# zfs destroy -f HD-Storage/vm-300-disk-1
root@pve1:/etc/pve/qemu-server# zfs destroy -f HD-Storage/vm-300-disk-0
```

### Supprimer la demande d'inscription 
[Enlever le message](https://dannyda.com/2020/05/17/how-to-remove-you-do-not-have-a-valid-subscription-for-this-server-from-proxmox-virtual-environment-6-1-2-proxmox-ve-6-1-2-pve-6-1-2/)  qui demande l'inscription lors de la connection GUI.

Fonctionne avec les versions suivantes de proxmox :
```bash

(6.2-15 6.3-2 6.3-3 6.3-4 6.3-6 6.3-7 6.4-4 6.4-5 6.4-6 6.4-7 6.4-8 6.4-9 6.4-10 6.4-11 7.0-5 7.0-6 7.0-8 7.0-9 7.0-10 7.0-11 7.0-13 7.0-14 7.0-14+1 7.1-4 7.1-5 7.1-6 and up)
```

* Version `6.1`
```bash
sed -i.backup "s/data.status !== 'Active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service
```
* Version `(6.2-12 et moins)`
```bash
sed -i.backup -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status.toLowerCase() \!== 'active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service
```
* Version `(6.2-12 and plus)`
```bash
sed -i.backup -z "s/res === null || res === undefined || \!res || res\n\t\t\t.data.status \!== 'Active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service
```


## Proxmox Backup Server

La documentation de [Proxmox Backup Server](https://www.proxmox.com/en/proxmox-backup-server) étant bien faite, 
je vous la recommande pour [l'installation ainsi que les prérequis](https://pbs.proxmox.com/docs/installation.html))

[Pour son Architecture](https://pbs.proxmox.com/docs/introduction.html#architecture), PBS utilise le [modèle client serveur](https://en.wikipedia.org/wiki/Client-server_model). 


