---
sidebar_position: 3
---


# Build

## Google Cloud Plateform - GCP 
Avec de l'Infrastructure As A Code, nous verrons depuis la console GCP :
* Build et push nos images dans l'artifact Registry 

**Prérequis**, s'authentifier :
```bash
gcloud auth configure-docker <Localisation> # Exemple :europe-west9-docker.pkg.dev
```
### [Build et push sur l'artifacte registry](https://cloud.google.com/build/docs/deploying-builds/deploy-cloud-run#code_examples) 
1. Pour constuire et pousser l'image, creer ou mettre a jour `cloudbuild.yaml`  
```bash
steps:
- name: 'gcr.io/cloud-builders/docker'
  args: [ 'build', '-t', 'europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc', 'deploydocu/.' ]

- name: 'gcr.io/cloud-builders/docker'
  args: ['push', 'europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc']
``
```
gcloud builds submit --config cloudbuild.yaml
``` 
**Il est possible de le faire manuellement** :
```bash
gcloud auth configure-docker europe-west9-docker.pkg.dev
docker build -t docu:0.1.0 deploydocu/.
docker tag docu:0.1.0 europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5
docker push europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5
```
### Création d'un service avec `Terraform`
* [Liens pour la documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#example-usage---cloud-run-service-basic)
* Création d'un service aec les droits IAM pour accèder depuis l'extérieur :
```YAML
provider "google" {
    project = "iaacgitlab"
}

resource "google_cloud_run_service" "default" {
    name     = "pablogrondin"
    location = "europe-west9"

    metadata {
      annotations = {
        "run.googleapis.com/client-name" = "terraform"
      }
    }

    template {
      spec {
        containers {
          image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"
        }
      }
    }
 }

 data "google_iam_policy" "noauth" {
   binding {
     role = "roles/run.invoker"
     members = ["allUsers"]
   }
 }

 resource "google_cloud_run_service_iam_policy" "noauth" {
   location    = google_cloud_run_service.default.location
   project     = google_cloud_run_service.default.project
   service     = google_cloud_run_service.default.name

   policy_data = data.google_iam_policy.noauth.policy_data
 }
```
Depuis la console GCP, utiliser `terraform init`, `plan`, `apply`

### Création d'un service service.yaml 
Parmis les façon de configurer GCP, il est possible d'utiliser les services.yaml. De nombreuses configurations par défaut comme les limites CPU, Mémoire n'ont pas besoins d'être spécifié.Nous additionons une révision 0.1.2 ci dessous. 
```YAML
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: deploydocu

spec:
  template:
    metadata:
      name: 0.1.2
    spec:

      containers:
      - image: europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5 
```
* Création du service :
```bash
gcloud run services replace service.yaml
```

## Gitlab 

### Contruire un conteneur et enregistrement dans le registry
* Utiliser la *Gitlab repository* afin d'héberger ses conteneurs 
```bash
build:
  stage: build
  image: docker
  services:
    - docker:18-dind
  script: 
    
    - docker info
---
sidebar_position: 3
---


# Build

## Google Cloud Plateform - GCP 
Avec de l'Infrastructure As A Code, nous verrons depuis la console GCP :
1. Build et push nos images dans l'artifact Registry 
2. Créer et déployer un service 
   * Avec `Terraform` 
   * Avec `knative`

L'ensemble de l'infrastructure va se gérer avec 2 fichiers : `cloudconfig.yaml`, `main.tf` ou `service.yaml`. GCP met aussi à disposition une CLI et un GUI.  

**Prérequis**, s'authentifier :
```bash
gcloud auth configure-docker europe-west9-docker.pkg.dev
```
### [Build et push sur l'artifacte registry](https://cloud.google.com/build/docs/deploying-builds/deploy-cloud-run#code_examples) 
1. Pour constuire et pousser l'image, creer ou mettre a jour `cloudbuild.yaml`  
```bash
steps:
- name: 'gcr.io/cloud-builders/docker'
  args: [ 'build', '-t', 'europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc', 'deploydocu/.' ]

- name: 'gcr.io/cloud-builders/docker'
  args: ['push', 'europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc']
```
2. Exécuter le build et push:
```
gcloud builds submit --config cloudbuild.yaml
``` 
**Il est possible de le faire manuellement** :
```bash
gcloud auth configure-docker europe-west9-docker.pkg.dev
docker build -t docu:0.1.0 deploydocu/.
docker tag docu:0.1.0 europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5
docker push europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc:0.1.5
```

## Gitlab 

### Contruire un conteneur et enregistrement dans le registry
* Utiliser la *Gitlab repository* afin d'héberger ses conteneurs 
```bash
build:
  stage: build
  image: docker
  services:
    - docker:18-dind
