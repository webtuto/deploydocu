---
sidebar_position: 1
---
# Truc et astuce 

:::warning
Bien que non *designer*, vous trouverez quelques truc et astuce afin de vous aidez à publier du contenu
::: 
## Contenu pour les sites web

1. Ressources 
 * [Des icones variées et colorés](https://www.flaticon.com/)
 * [Conversion PNG en SVG](https://www.aconvert.com/fr/image/png-to-svg/). Si comme vous appréciez les images PNG de flaticon et rechercher un moyen de conversion en SVG de qualité
