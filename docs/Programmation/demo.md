---
sidebar_position: 1
---
# Python, les bases
Ce tutoriel rappelle les bases du langage python et détaille des *tips* avec des exemples afin de manipuler les `list`, `dict`, `set` et fonctions python. Pour plus d'information concernant python, [la doc](https://docs.python.org/2/library/sets.html) 
## Les variables 

* Les *tuples* de forme : nomVar =  (arg1,arg2,argn) sont plus rapides à parcourir que les listes et sont immuables. Un *tuple* peut contenir une *list*. Dans ce cas, il n'est plus immuable. C'est possible car les *tuples* contiennent la référence aux objets qu'il contient (hé oui, python est un langage de POO). 

* Les *dictionnaires* associent `{key:value}` avec key qui doit être de type immuable. 

* Les *set* qui contiennent une collection non ordonnée d'éléments uniques. Ecrit de forme `var = set([1,2,3])`. Pratique pour supprimer les doublons. *ImmutableSet* mais aussi des objets immuable à disposition. [La doc](https://docs.python.org/2/library/sets.html)    

Quelques mots clefs utiles :  `eval`,`dir`,`type`

```python
# Base
# Modulo : Rest de la division euclidienne
10%5 # 0 
16%3 # 1 

# Division  
11/5 #2.2
11//5 # 2


tuple = (1,2,3)
dictionnaires = {"key":10} # Peut aussi s'écrire avec la méthode : 
 a = dict(one=1, two=2, three=3)
d1 = {"a":3}

# On peut passer un tuple en clef d'un dictionnaire : 
d = {("a",1),("b",3),("c",10)}

# Les Set : 
s = set([10,1,1,2,3]) # renverra sans le double 1. Le set sera dans un ordre aléatoire. 

```

1. Copier les valeurs des `list` ou `set`
    * `.copy()`, `[:]`
    * `[::-1]` ordre inversé de la liste
2. Copier les valeurs des  `dict`
    * Copier `{**<NOM_DICT>}`
    * [Trier un dictionnaire](https://hackernoon.com/python-tricks-101-2836251922e0) 
    * Affecter des variables 
```python
# 1. Les listes 
t1 = [1,2,3,4,5]
t2 = t1.copy()
t3 = t1[:] 
# Ordre inversé avec ::-1
inverse_t1 = t1[::-1]
>>> t3
[1, 2, 3, 4, 5]
>>> inverse_t1
[5, 4, 3, 2, 1]

# Affecter des variables a partir d'une liste
a,b,*c = [1,2,85,46,70]

# 2. Pour les dictionnaire : 
d1 = {"a":56}
d2 = {**d1}
print(d2)
>>> print(d2)
{'a': 56}



```
## Les listes

### Fréquence des éléments
1. Trouver l'lélement le plus fréquent d'une liste
2. Manipuler les listes
3. La fréquence de chaque élément de la liste
```python
# 1. element les plus frequents : 
l =[1,1,101,102,4,8,8,8,4,12,9]
print(max(set(l),key=l.count))
8


l = [1,50,40,3,20]
print(l[:3]) # [1,50,40]
print(l[2:]) # [40,3,20]

# 3. La fréquence de chaque élément 
l =[1,1,101,102,4,8,8,8,4,12,9]
print(max(set(l),key=l.count))
print(set(l))
print(max(l,key=l.count))
freq = [(item,l.count(item)) for item in l]
print(freq)
mylist = list(dict.fromkeys(freq))
print(mylist)

```

### Liste imbriqué
```python
# On met tout ce qu il y a dans les listes au carre :
l1 = [1,2,3]
l2 = [4,5,6]
L = [l1,l2]

print(L)
carre = [[i**2 for i in sublist] for sublist in L]
print(carre)
# On peut aussi faire :
carre = [j**2 for i in L for j in i]
print(carre)
```

### Intersection de listes

```python
lst1 = [15, 9, 10, 56, 23, 78, 5, 4, 9]
lst2 = [9, 4, 5, 36, 47, 26, 10, 45, 87]

commun = [i for i in lst1 if lst1 in lst2]
print(commun)
```
Renvoi true ou false si dans la liste : 
```python
lst1 = [15, 9, 10, 56, 23, 78, 5, 4, 9]
lst2 = [9, 4, 5, 36, 47, 26, 10, 45, 87]

commun = [i in lst2 for i in lst1]
print(commun)
```

## Dictionnaire

1. Enlever des items avec la clef

```python
# Traitement pour dictionnires
print(SommeCarre)
print("###################")
Acteursbrute = {
	1: 'Chuck Noris',
	2: 'Turtle Doves',
	3: 'French Hens',
	4: 'Colly Birds',
	5: 'Gold Rings',
	6:'Geese-a-Laying',
	7: 'Swans-a-Swimming',
	8: 'Maids-a-Milking',
	9: 'Ladies Dancing'
}

# 1. Enlever des items
KeyAEnleve = [1,5]


for key in KeyAEnleve:
	if key in Acteursbrute:
		del Acteursbrute[key]

print(Acteursbrute)
print("###########################################")
Acteursbrute = {
	1: 'Chuck Noris',
	2: 'Turtle Doves',
	3: 'French Hens',
	4: 'Colly Birds',
	5: 'Gold Rings',
	6:'Geese-a-Laying',
	7: 'Swans-a-Swimming',
	8: 'Maids-a-Milking',
	9: 'Ladies Dancing'
}

l = [acteur for acteur in Acteursbrute.items() if acteur[0] not in KeyAEnleve]

# La meme chose 
l = [{k,v} for k,v in Acteursbrute.items() if k not in KeyAEnleve]
#list(l)
print(Acteursbrute)
print("#####")
print(list(l))
```



## Générateur d'expression

Vient remplacer les map reduce filter en python.  

```python
"""
on peut faire des operation imbriquer en une seule ligne
https://www.programiz.com/python-programming/generator
Ca s'appelle les generator expression
"""
nombres = [1,45,60,100,30]

# Generator expresion
SommeCarre = (sum(x*2 for x in nombres if x > 50)) # Ici 100*2 + 60*2

# List expresion :
SommeCarre = [sum(x*2 for x in nombres if x > 50)]

```

## Opération de comparaison == vs is
* `==` compare les valeurs
* `is` compare les références

```python
nom = [1,5,56,98,45]
nom2= nom # On copie les références 
print(id(nom))
print(id(nom2))

print(nom == nom2) # True
print(nom is nom2) # False
# Comparson les id :

print("##########")
prenom = [1,5,56,98,45]
print(nom == prenom) # True
print(nom is prenom) # False

```
## Hashability 

Tous les objets immuables sont hashables. 

```python
hello = ('salut','Pablo')
helloB = ('salut','Pablo')
print(hash(hello))
print(hash(helloB))

print(hash(hello) == hash(helloB))
d = {hello:3} # marche car hello immuable
e = {d:4} # Ne marche pas car d est un dictionnaire donc non immuable

```
Un dictionnaire ne peut prendre en clefs que les éléments hashable, un `set` qui contient des éléments hashable ne le sera plus. 
## Fonctions 

Les fonctions sont définies par `def`

### Arguments de fonctions *args et **kargs 
* `args` est un tuple 
*  `kargs` un dictionnaire.
```python

def presentation(title, **kwargs):
    print ('---- %s ----' %(title))

    # None est la valeur par defaut si l utilisateur ne rentre rien
    name=kwargs.get('name', None)
    print("kwargs est de type : {} ".format(type(kwargs)))
    firstname=kwargs.get('firstname', None)

    if (name and firstname):
        print('mon nom est %s, %s %s' %(name, firstname, name))
    elif (firstname and not(name)):
        print('Mon prenom est %s' %(firstname))
    elif (not(firstname) and name):
        print('Mon nom est %s' %(name))
    else:
        print('Je ne suis personne...')

presentation('Prenom+nom', firstname='James')

def pres(titre,*args):
    print(type(args))
    for key,value in enumerate(args):
        print("le titre est : {} et la valeur est {}".format(titre,value))

def pres2(titre,*args):
    print(type(args))
    for value in args:
        print("le titre est : {} et la valeur est {}".format(titre,value))

pres("Jean",1,4,5)
print("####################")
pres2("Jean",1,4,5)

```


### Closure en python 
Fonction définie dans une autre fonction qui utilise une variable définie dans la fonction "au dessus".   

Mot clef `nonlocal` pour utiliser la variable "mere"

```python
def compteur():
    n = 0
    def add():
        nonlocal n
        n+=1
        return n
    return add

a = compteur()
print(a(),a(),a(),sep='\n')

def cpt():
    return lambda valeur : valeur + 1

a = cpt()
print(a(1),a(2),a(1),sep='\n')

```  
### CallBack 
Fonction appelé en parametre, qui va être appelé selon une condition. 
```python
def f(a):
    return a**2

def utiliseF(x,fonction):
    return fonction(x)
    
print(utiliseF(5,f))
```

### Lambda 
* Ce sont des fonctions anonymes
* La fonction s'écrit sur une ligne
* Une instruction dans la fonction

```python
"""

    On ne peut les écrire que sur une ligne.
    On ne peut pas avoir plus d’une instruction dans la fonction.

"""

x = lambda a, b : a * b
print(x(5, 6))

def calculPuissance(x):
	"""Je retourne le carre"""
	return lambda i: x**i

p1  = calculPuissance(2)
print(p1(3))
p2 = calculPuissance(2)
print(p2(10))

# On peut ecrire des choses comme ca :
print((lambda x: 2**x)(3))


# Structure conditionnelle : 
majorite = lambda x : "majeur" if x>18 else "mineur"
print(majorite(15))
print(majorite(20))

# Recursivite :
fib = lambda n : n if n<2 else fib(n-1) + fib(n-2)

print(fib(10))
```
### Effet de bord
Notamment car l'évaluation des parametres se fait en statique
```python
def a():
   print("hello")
   return 3
def f(x, y, z):
   print("begin")
   print(x, y, z, sep='\n')
   print("end")

def b():
   print("goodbye")
   return 5
f(a(), b(), b())

```
