---
sidebar_position: 1
---

# Terraform GCP
[Terraform sur GCP](https://cloud.google.com/docs/terraform?hl=fr) permet de provisionner des VM, Conteneur, mise en réseau et stockage à l'aide de fichier déclaratif. 

Google cloud propose une [suite complète](https://cloud.google.com/architecture/managing-infrastructure-as-code?hl=fr) pour la gestion Iac : 
* Github pour le versionning, gestion des branches qui deviendront potentionnelent des envrionnements de production, de dev, etc
* Terraform pour la gestion de l'infrastructure sur GCP
* Cloud build en service d'intégration continue pour appliquer automatiquement Terraform
* [Lien github pour la suite implémentant les  pratiques Gitops avec de l'infrastructure as a code Terraform](https://github.com/GoogleCloudPlatform/solutions-terraform-cloudbuild-gitops)

## Les possibilités de Terrafoirm sur GCP
[De nombreux modules sont proposés](https://cloud.google.com/docs/terraform/blueprints/terraform-blueprints). Quelque modules :
* [Cloud run](https://github.com/GoogleCloudPlatform/terraform-google-cloud-run)
   * [Les connecteur VPC](https://github.com/terraform-google-modules/terraform-google-log-export)  pour connecter les instance Cloud Run à tout se qui à une IP en interne.
* [Jenkins](https://github.com/terraform-google-modules/terraform-google-jenkins)
* Réseau
   * [Télémétrie](https://cloud.google.com/architecture/deploy-network-telemetry-blueprint). Attention au cout, cela crée des VM. 
   * [Parefeux](https://github.com/terraform-google-modules/terraform-google-network/tree/master/modules/firewall-rules)
* [Stockage](https://github.com/terraform-google-modules/terraform-google-cloud-storage)
* Cloud opération, pour les VM. 

## Importer des ressources existantes 
Afin de **partir de l'infrastructure existante**, il est possible de générer du code terraform à partir de [gcloud](https://cloud.google.com/docs/terraform/resource-management/import?hl=fr). puis de créer des modules à partir du code généré. 

## Cloud Run Terraform
* [Lien github pour le module cloud run sur terraform](https://github.com/GoogleCloudPlatform/terraform-google-cloud-run)
* [Lien de la documentation terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service)
* [Terraform et GCP](https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-change?in=terraform/gcp-get-started)
* [Configurer les spec](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#nested_spec)
* [Stocker l'état de Terraform dans un bucket Cloud Storage](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket)
   * Travail à distance 
   * Travail collaboratif
   * Backup et versionning des states.
* [Terraform doc Cloud Storage bucket](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket)
### Stocker l'état de Terraform dans un bucket

[La documentation](console.cloud.google.com/artifacts/docker/iaacgitlab/europe-west9/docusaurus/pablogrondindoc?project=iaacgitlab) décrit très bien les démarches à effectué. A noter qu(il n'y a pas besoin d'IAM si lancé depuis la console. Vous devriez avoir cela une fois `terraform init`effectué :
```bash
Successfully configured the backend "gcs"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Reusing previous version of hashicorp/google from the dependency lock file
- Using previously-installed hashicorp/google v4.29.0

Terraform has been successfully initialized!
```
* Le bucket est disponible dans ̀`Cloud Storage` ave les états `tfstate` et `tfstate.backup` 
### Elements principales de configurations des conteneurs 

Un conteneur va avoir un nombre minimum d'instance, si 0 aucune ressources est consommé si il n'y a pas d'activité. 

Un conteneur à un nombre maximal de theard (pour le web requete) qu'il va accepté avant de créer une nouvelle instance docker. Cela s'applique uninquement au conteneur sans état. Dans `spec`, `container_concurrency`, par défaut de 80. 

Un conteneur à de la puissance CPU et RAM alloué, par défaut de 512 Mi et 1000m pour le CPU. 
### Déploiement d'un container de l'Artifact registry
* Déployer depuis la documentation
```ỲAML
provider "google" {
    project = "iaacgitlab"
}

resource "google_cloud_run_service" "default" {
    name     = "pablogrondin"
    location = "europe-west9"

    metadata {
      annotations = {
        "run.googleapis.com/client-name" = "terraform"
      }
    }

    template {
      spec {
        containers {
          image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"
        }
      }
    }
 }

 data "google_iam_policy" "noauth" {
   binding {
     role = "roles/run.invoker"
     members = ["allUsers"]
   }
 }

 resource "google_cloud_run_service_iam_policy" "noauth" {
   location    = google_cloud_run_service.default.location
   project     = google_cloud_run_service.default.project
   service     = google_cloud_run_service.default.name

   policy_data = data.google_iam_policy.noauth.policy_data
 }
```

L'astuce pour avoir les config : avec cette 1ère configuration, on peut voir comment gérer les limites CPU, mémoire du contenreur avec un `terraform show`. On apprend que terraform utilise knative et affecte les valeurs suivantes par défaut pour un conteneur :
```YAML
            containers {
                image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"

                ports {
                    container_port = 8080
                    name           = "http1"
                }

                resources {
                    limits   = {
                        "cpu"    = "1000m"
                        "memory" = "512Mi"
                    }
                    requests = {}
                }
            }
```

## Gestion des rollings- Nouvelle version
   * Se fait à l'aide de `revision` ou `tag`.[Lien doc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service#containers)
   * Dans la balise `traffic`.
* En partant d'une version existante, déployer une nouvelle revision et lui affectuer une revision automatiquement
   * Il doit y avoir une nouvelle image dans le repository
```YAML

resource "google_cloud_run_service" "default" {
  name     = "pablogrondin"
  location = "europe-west9"

  metadata {
    annotations = {
      "run.googleapis.com/client-name" = "terraform"
    }
  }

  template {
    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = "0",
        "autoscaling.knative.dev/maxScale" = "10"
      }
      #generation = 0 #default
      labels = {} # default
          
      
    
    }
    spec {
      container_concurrency = 80 #default
      containers {
        image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"
        ports {
          container_port = 8080
          name           = "http1"
        }
        # Valeur par defaut
        resources {
          limits = {
            "cpu"    = "1000m"
            "memory" = "512Mi"
          }
        }

      }
    }
  }
  autogenerate_revision_name = true
  traffic {
    latest_revision = true
    percent         = 25

  }
traffic {
    percent         = 75
    revision_name = "pablogrondin-gpqrf"

  }

}

data "google_iam_policy" "noauth" {
  binding {
    role    = "roles/run.invoker"
    members = ["allUsers"]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_service.default.location
  project  = google_cloud_run_service.default.project
  service  = google_cloud_run_service.default.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
``` 
### Switch et répartition de la charge par révision
* Gérer le traffic entre plusieurs révision, une fois la nouvelle version déployé, on récupère le nom de la révision deluis `Service`> `Revision` :
   * **Enlever`autogenerate_revision_name = true`**
```YAML
  traffic {
    percent         = 25
    revision_name = "pablogrondin-997jk"

  }
traffic {
    percent         = 75
    revision_name = "pablogrondin-gpqrf"

  }
```
### Qualité de service - SLO - LA - SLI
* [Rapelle des concepts SLO - SLA - SLI](https://cloud.google.com/stackdriver/docs/solutions/slo-monitoring)
   * SLI : Indicateur de niveau de service : mesure des performances
   * SLO - Objectif de niveau de service : énoncé des performance souhaité
   * Marges d'erreur : Mesure entre performance souhaité et réelle

GCL propose un [imonitoring des SLO avec Terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_slo) en IAC.
* [L'api est définit en REST](https://cloud.google.com/monitoring/api/ref_v3/rest/v3/services.serviceLevelObjectives)
* 
## Valider la déclaration terraform code 
* [Formater et valider la configuration](https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-build?in=terraform/gcp-get-started)

``ỲAML
terraform fmt
terraform validate
```
## Les states 
```YAML
terraform show
```
