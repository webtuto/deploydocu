provider "google" {
    project = "iaacgitlab"
}

resource "google_cloud_run_service" "default" {
    name     = "pablogrondin"
    location = "europe-west9"

    metadata {
      annotations = {
        "run.googleapis.com/client-name" = "terraform"
      }
    }

    template {
      spec {
        containers {
          image = "europe-west9-docker.pkg.dev/iaacgitlab/docusaurus/pablogrondindoc"
        }
      }
    }
 }

 data "google_iam_policy" "noauth" {
   binding {
     role = "roles/run.invoker"
     members = ["allUsers"]
   }
 }

 resource "google_cloud_run_service_iam_policy" "noauth" {
   location    = google_cloud_run_service.default.location
   project     = google_cloud_run_service.default.project
   service     = google_cloud_run_service.default.name

   policy_data = data.google_iam_policy.noauth.policy_data
 }
