import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/__docusaurus/debug',
    component: ComponentCreator('/__docusaurus/debug', 'c3e'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/config',
    component: ComponentCreator('/__docusaurus/debug/config', 'f72'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/content',
    component: ComponentCreator('/__docusaurus/debug/content', '24b'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/globalData',
    component: ComponentCreator('/__docusaurus/debug/globalData', 'cbf'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/metadata',
    component: ComponentCreator('/__docusaurus/debug/metadata', 'e03'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/registry',
    component: ComponentCreator('/__docusaurus/debug/registry', '207'),
    exact: true
  },
  {
    path: '/__docusaurus/debug/routes',
    component: ComponentCreator('/__docusaurus/debug/routes', '043'),
    exact: true
  },
  {
    path: '/blog',
    component: ComponentCreator('/blog', '811'),
    exact: true
  },
  {
    path: '/blog/archive',
    component: ComponentCreator('/blog/archive', 'a5b'),
    exact: true
  },
  {
    path: '/blog/first-blog-post',
    component: ComponentCreator('/blog/first-blog-post', 'f34'),
    exact: true
  },
  {
    path: '/blog/long-blog-post',
    component: ComponentCreator('/blog/long-blog-post', '654'),
    exact: true
  },
  {
    path: '/blog/mdx-blog-post',
    component: ComponentCreator('/blog/mdx-blog-post', 'c40'),
    exact: true
  },
  {
    path: '/blog/tags',
    component: ComponentCreator('/blog/tags', '9bc'),
    exact: true
  },
  {
    path: '/blog/tags/docusaurus',
    component: ComponentCreator('/blog/tags/docusaurus', '34a'),
    exact: true
  },
  {
    path: '/blog/tags/facebook',
    component: ComponentCreator('/blog/tags/facebook', 'c7c'),
    exact: true
  },
  {
    path: '/blog/tags/hello',
    component: ComponentCreator('/blog/tags/hello', '2a1'),
    exact: true
  },
  {
    path: '/blog/tags/hola',
    component: ComponentCreator('/blog/tags/hola', 'f3f'),
    exact: true
  },
  {
    path: '/blog/welcome',
    component: ComponentCreator('/blog/welcome', '58a'),
    exact: true
  },
  {
    path: '/markdown-page',
    component: ComponentCreator('/markdown-page', 'eb3'),
    exact: true
  },
  {
    path: '/docs',
    component: ComponentCreator('/docs', '25a'),
    routes: [
      {
        path: '/docs/category/docusaurus-tutorial---extras',
        component: ComponentCreator('/docs/category/docusaurus-tutorial---extras', 'aa5'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/category/tuto---cicd',
        component: ComponentCreator('/docs/category/tuto---cicd', 'c7b'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Conteneurisation/Build',
        component: ComponentCreator('/docs/Conteneurisation/Build', '3bf'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Conteneurisation/commande_docker',
        component: ComponentCreator('/docs/Conteneurisation/commande_docker', 'bf0'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Conteneurisation/container_virt',
        component: ComponentCreator('/docs/Conteneurisation/container_virt', '819'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/designweb/demo',
        component: ComponentCreator('/docs/designweb/demo', '02a'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Iac/Terraform-GCP',
        component: ComponentCreator('/docs/Iac/Terraform-GCP', '8cf'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/intro',
        component: ComponentCreator('/docs/intro', 'aed'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Pipelines/GCP_CD',
        component: ComponentCreator('/docs/Pipelines/GCP_CD', '366'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Pipelines/Gitlab_CI',
        component: ComponentCreator('/docs/Pipelines/Gitlab_CI', '40c'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Pipelines/Render_CD',
        component: ComponentCreator('/docs/Pipelines/Render_CD', '505'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Programmation/demo',
        component: ComponentCreator('/docs/Programmation/demo', '6b5'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/system/debug',
        component: ComponentCreator('/docs/system/debug', '4f4'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/system/demo',
        component: ComponentCreator('/docs/system/demo', '4dd'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/tutorial-extras/manage-docs-versions',
        component: ComponentCreator('/docs/tutorial-extras/manage-docs-versions', 'fdd'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/tutorial-extras/translate-your-site',
        component: ComponentCreator('/docs/tutorial-extras/translate-your-site', '2d7'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Virtualisation/Hyperviseur_type_2',
        component: ComponentCreator('/docs/Virtualisation/Hyperviseur_type_2', '5d7'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/docs/Virtualisation/Hyperviteur_type_1',
        component: ComponentCreator('/docs/Virtualisation/Hyperviteur_type_1', '157'),
        exact: true,
        sidebar: "tutorialSidebar"
      }
    ]
  },
  {
    path: '/',
    component: ComponentCreator('/', '65e'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
