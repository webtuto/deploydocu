# Intro
Objectif :
1. Déployer sur render.com
2. Déployer sur cloud run
## Déployer depuis Render
Déploiement depuis push sur le repo *gilab* avec un push
```bash
git config --global user.name "<USERNAME_GITLAB>"
git config --global user.email "<MAIL_GITLAB>"
ssh-keygen -t rsa -b 4096 -C "<MAIL_GITLAB>"
eval "$(ssh-agent -s)"
ssh-add /root/.ssh/<CLEF_SSH_PRIVE>"
ssh -T git@gitlab.com
git clone git@project
```
## Déployer sur Cloud run 

Tout se fait coté GCP : Build, Push et mise en service. 
1. Récupération du repo 
```bash
git clone --branch cloudrun https://gitlab.com/webtuto/deploydocu.git
```
2. Authentification
```
gcloud auth configure-docker europe-west9-docker.pkg.dev
``` 
3. Build et push sur artifact repository 
* Le fichier *cloudbuild.yaml* contient le build et le push.
* Il est a editer manuelle a chaque push
```bash
gcloud builds submit --config cloudbuild.yaml
```
4. façons de mettre en servicee fonctionnel :
   * Utiliser *Terraform* : `,init`, `plan`, `apply`
   * Utiliser service.yaml :
```bash
gcloud run services replace service.yaml
```
### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

Using SSH:

```
$ USE_SSH=true yarn deploy
```

Not using SSH:

```
$ GIT_USER=<Your GitHub username> yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
