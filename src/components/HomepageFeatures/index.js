import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';


const FeatureList = [
  {
    title: 'Auto-entrepreneur',
    Svg: require('@site/static/img/freelance.svg').default,
    description: (
      <>
          Contactez-moi pour des prestations en systèmes et réseaux.
          Mais ça correspond à quoi au fait ?
          Je réponds aux enjeux et problématiques des sociétés en apportant des solutions techniques.   
          Retrouvez-moi sur mon profil Malt <a href='https://www.malt.fr/profile/pablogrondin'><img src={require('@site/static/img/Malt-icon.png').default} alt="Profil Malt de Pablo Grondin" /> </a>


      </>
    ),
  },
  {
    title: 'Du contenu varié',
    Svg: require('@site/static/img/merge.svg').default,
    description: (
      <>
      Vous trouverez du contenu principalement technique en systèmes et réseaux : Des languages et paradygmes de programmation, le cloud, des objets connectés la conception et l'utilisation
      d'IA basé sur l'apprentissage automatique et des outils moins techniques, et bien plus encore ! 
      
      </>
    ),
  },
  {
    title: 'Ce site, Docusaurus  et Gitlab',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        <a href= 'https://docusaurus.io/fr/'><img src={require('@site/static/img/favicon.ico').default} alt="Docousaurus"/> </a> Docusaurus promet de construire des sites rapidement et optimisés afin de nous concentrer sur notre
        contenu. 

        Associé à <a href= 'https://gitlab.com/webtuto/pablogrondindoc'><img src={require('@site/static/img/favicon-webp-gitlab.webp').default} alt="Gitlab"  /> </a> Gitlab pour la contruction d'une Pipeline CI et la mise ligne avec les Gitlab page, la promesse est tenue !  
        
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
