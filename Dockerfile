FROM node:18-alpine3.15
COPY . ./
RUN yarn install
RUN yarn build 
CMD ["yarn","start","--port","8080","--host","0.0.0.0"]

